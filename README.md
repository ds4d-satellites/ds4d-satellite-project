# DS4D Satellite Project

[TOC]

## tests
test from tianhao

test add

another test

what is commit?--Molin

haili is learning how to do branch--Haili

This is another test

Test with atom--Molin

Test with VScode--Molin

Test with VScode--Haili

Test VScode branch--Mo

Test VScode branch and upload--Haili

Test atom branch and push--Haili
## Assignment 3 Brief

Create a piece that communicates about the data you have, including developing the context and exploring implications. This assignment is carried out as a **group project**, with a grade per group against three learning outcomes (Data, Program, Communicate).

This is an open brief, which means you can take whatever approach is appropriate and use whatever techniques are available to your group to understand your data and communicate your findings. Previous submissions have included reports, interactive websites, physicalisations, information videos, data comics and more. You will have a chance to present this in Week 11 (3rd of December) shortly before handing in.

Any medium is permissible, subject to space/time/technical/resource constraints, for example:
- interactives,
- websites,
- data comics,
- infographics,
- AR/VR,
- physicalisations,
- dance/theatre work
- etc.

### Submission
This is a Group submission - only one person from each group needs to submit. You should submit:

* Slides from your presentation

**AND EITHER:** A 3000 word PDF report (if you are presenting scientific knowledge to a scientific audience). 4. This should contain appropriate figures and statistics to support communication to a critical, data literate audience.

**OR:** A 1000 word PDF report and some form of documentation of your creation. The creation could be a 2 minute video, or a link to a live interactive, The report should discuss the context and findings, and is used to back up all of the information not present in the interactive piece. Links to the video are fine if it cannot be uploaded to Learn.

### Unpacking Learning Outcomes

Your work relates to the learning outcomes in a similar way to Assignment 1:

* **LO1: Data** - show that you are aware of issues surrounding the data - social context, problems that may exist, bias, underrepresentation, missing data and so on. Demonstrate that you are handling and processing data correctly.
* **LO2: Program** - carry out an appropriate computational processing of the data using relevant computational tools.
* **LO3: Communicate** - Communicate your findings. This is a combination of how well your artefact communicates to a target audience and how well your presentation communicates your process and findings.
