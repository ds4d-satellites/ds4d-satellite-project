# How to upload video to the website
1. Upload video to Media Hopper
2. Set the privacy setting to **Unlisted**
3. Set "Retain Source File" to Yes
4. Go to Media and press the edit button (probably under actions)
5. Go to Downloads tab, select Source and Save
6. Go to Media again, and this time there should be a Downloads tab too.
7. Right click the icon under Actions and select "Copy link"
8. Now go to the index.html file of our project
9. You should be able to see the video src right away with a link or a file address
10. Paste the link from Media Hopper there
11. Commit changes
12. Now open FileZilla and connect to the server
13. Replace the index.html file with the new one and you're done
