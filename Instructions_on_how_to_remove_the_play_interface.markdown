Audio playing:

<audio id="audio" src="images/openspeech.mp3" style="opacity:0" preload="auto" controls loop hidden="true"/>  
 
if it is not playing than use this code:
<var myAuto = document.getElementById('audio');  
myAuto.play();  

 

Other properties explain:

preload: If this property occurs, the audio loads when the page loads and is ready to play. If you use "autoplay", the property is ignored.
autoplay: If this property occurs, the audio plays as soon as it is ready. Code: autoplay="autoplay" can also be used: autoplay="true" 
Controls: If this property occurs, play buttons are displayed to the user.
Loop: If this property appears, playback resumes whenever the audio ends.
Hidden: Hide the play interface