# Poem

in dark stage, a thousand lights shine bright

some dazzling balls of fire, lifetimes away

others, pieces of human ingenuity, monuments to curiosity and connection

yet what might happen, as humanity reaches for the sun?



in foggy stage, a million lights shine bright

pieces of human ingenuity, husks of metal dead.

a littering of broken shards

oh, dazzling ancestors, where have you gone?

all we can see now, is what we have done 
